/**
 * View Models used by Spring MVC REST controllers.
 */
package pl.com.skrotowiec.web.rest.vm;
